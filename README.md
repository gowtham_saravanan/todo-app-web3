# To-Do List Management Project

ToDo is a decentralised app to store our daily tasks. We can create new tasks, edit and delete them.

The access to the application, our metamask wallet should be connected and the user should own our membership ERC-721 NFT. The first 1000 NFT's are free, after that 0.001 ether is charged.

The todo app is run by community with the help of DAO. The governance is established via ERC-20 token (TD), Timelock and Governor contract.


# Technologies Used

- Solidity
- Ether.js
- React.js
- HardHat
- Tailwind

Application Deployed to - https://todo-dapp.netlify.app/


## Images

![welcome-page](client/public/welcome page.png?raw=true "Welcome Page")
![home-page](client/public/home-page.png?raw=true "Home Page")
