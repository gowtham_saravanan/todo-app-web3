import dotenv from "dotenv";
dotenv.config();

async function main() {
  const TodoGovernor = await hre.ethers.getContractFactory("TodoGovernor");
  const todoGovernor = await TodoGovernor.deploy(process.env.TOKEN_ADDRESS, process.env.TIMELOCK_ADDRESS);

  await todoGovernor.deployed();

  console.log("Todo Governor deployed to:", todoGovernor.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
