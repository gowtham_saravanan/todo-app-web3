async function main() {
  const TodoMembershipNft = await hre.ethers.getContractFactory("TodoMembershipNft");
  const membershipNft = await TodoMembershipNft.deploy();

  await membershipNft.deployed();

  console.log("Membership NFT deployed to:", membershipNft.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
