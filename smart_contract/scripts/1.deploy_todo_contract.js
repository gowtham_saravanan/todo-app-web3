async function main() {
  const TodoList = await hre.ethers.getContractFactory("TodoList");
  const todo = await TodoList.deploy();

  await todo.deployed();

  console.log("TodoList deployed to:", todo.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
