import dotenv from "dotenv";
dotenv.config();

async function main() {
  
  const token = await hre.ethers.getContractAt("TodoToken", process.env.TOKEN_ADDRESS);

  let tokenOwner = await token.owner();
  console.log("Owner of Token - ", tokenOwner);

  if(tokenOwner != process.env.TIMELOCK_ADDRESS){
    console.log("Transferring ownnership of token to timelock");
    let txn1 = await token.transferOwnership(process.env.TIMELOCK_ADDRESS)
    await txn1.wait();
    tokenOwner = await token.owner();
    console.log("Token Ownership transferred to Timelock. Current owner of token - ", tokenOwner);  
  }
  
  const timelock = await hre.ethers.getContractAt("TodoTimelock", process.env.TIMELOCK_ADDRESS);
  const proposerRole = await timelock.PROPOSER_ROLE();
  console.log("Proposer role", proposerRole);
  const executerRole = await timelock.EXECUTOR_ROLE();
  const adminRole = await timelock.TIMELOCK_ADMIN_ROLE();



  let hasRole = await timelock.hasRole(proposerRole, process.env.WALLET_ADDRESS)
  console.log("Checking if governor has propser role - ", hasRole);
  if(!hasRole){
    console.log("Providing propser role to governor");
    let txn = await timelock.grantRole(proposerRole, process.env.WALLET_ADDRESS);
    await txn.wait();
    hasRole = await timelock.hasRole(proposerRole, process.env.WALLET_ADDRESS)
    console.log("Checking if governor has propser role - ", hasRole);  
  }

  hasRole = await timelock.hasRole(proposerRole, process.env.GOVERNOR_ADDRESS)
  console.log("Checking if governor has propser role - ", hasRole);
  if(!hasRole){
    console.log("Providing propser role to governor");
    let txn = await timelock.grantRole(proposerRole, process.env.GOVERNOR_ADDRESS);
    await txn.wait();
    hasRole = await timelock.hasRole(proposerRole, process.env.GOVERNOR_ADDRESS)
    console.log("Checking if governor has propser role - ", hasRole);  
  }
  
  hasRole = await timelock.hasRole(executerRole, hre.ethers.constants.AddressZero)
  console.log("Checking if anyone has executor role - ", hasRole);
  if(!hasRole){
    console.log("Providing executor role to anyone");
    let txn = await timelock.grantRole(executerRole, hre.ethers.constants.AddressZero);
    txn.wait();
    hasRole = await timelock.hasRole(executerRole, hre.ethers.constants.AddressZero)
    console.log("Checking if anyone has executor role - ", hasRole);  
  }
 
  hasRole = await timelock.hasRole(adminRole, process.env.WALLET_ADDRESS);
  console.log("Checking if we have admin role of timelock - ", hasRole);
  if(hasRole){
    console.log("Revoking admin role in timelock");
    let txn = await timelock.revokeRole(adminRole, process.env.WALLET_ADDRESS);
    await txn.wait();
    hasRole = await timelock.hasRole(adminRole, process.env.WALLET_ADDRESS)
    console.log("Checking if we have admin role of timelock - ", hasRole);
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
