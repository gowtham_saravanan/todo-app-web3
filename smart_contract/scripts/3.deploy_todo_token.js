async function main() {
  const TodoToken = await hre.ethers.getContractFactory("TodoToken");
  const todoToken = await TodoToken.deploy();

  await todoToken.deployed();

  console.log("Todo Token deployed to:", todoToken.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
