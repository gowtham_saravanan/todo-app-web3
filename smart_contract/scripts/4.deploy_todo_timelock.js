async function main() {
  const TodoTimelock = await hre.ethers.getContractFactory("TodoTimelock");
  const todoTimelock = await TodoTimelock.deploy(60, [], []);

  await todoTimelock.deployed();

  console.log("Todo Timelock deployed to:", todoTimelock.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
