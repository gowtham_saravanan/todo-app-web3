require("@nomiclabs/hardhat-waffle");

module.exports = {
  solidity: "0.8.4",
  networks: {
    ropsten : {
      url : 'your alchemy app webhook',
      accounts : ['your-private-key']
    }
  }
};
