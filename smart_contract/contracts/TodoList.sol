// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.4;

contract TodoList {

    enum Status {PENDING, STARTED, COMPLETED }

    uint256 lastTaskId = 0;

    struct Todo {
        uint id;
        string task;
        Status status;
        address owner;
    }

    mapping(address => uint[]) addressTaskMapping;
    
    mapping(uint => Todo) toDoLists;

    modifier ownsTask(uint id) { 
        require(toDoLists[id].owner == msg.sender, 'Permission Denied');
        _;
    }

    function getMyLists() public view returns (Todo[] memory) { 
        uint totalLists = addressTaskMapping[msg.sender].length;
        Todo[] memory myLists = new Todo[](totalLists);

        for(uint i=0; i<totalLists; i++ ){
            myLists[i] = toDoLists[addressTaskMapping[msg.sender][i]];
        }
        return myLists;
    }

    function getTask(uint id) ownsTask(id) public view returns (Todo memory) {
        return toDoLists[id];
    }    

    function add(string memory task) public{
        uint id = ++lastTaskId;
        addressTaskMapping[msg.sender].push(id);
        toDoLists[id] = Todo(id, task, Status.PENDING, msg.sender);
    }

    function updateTask(uint id, string memory task, Status status) ownsTask(id) public{
        toDoLists[id].task = task;
        toDoLists[id].status = status;
    }

    function deleteTask(uint id) ownsTask(id) public{
        bool taskFound = false;
        for(uint i=0; i<addressTaskMapping[msg.sender].length; i++){
            if(!taskFound && addressTaskMapping[msg.sender][i] == id){
                taskFound = true;
            }
            if(taskFound && i!=addressTaskMapping[msg.sender].length-1){
                addressTaskMapping[msg.sender][i] = addressTaskMapping[msg.sender][i+1];
            }
        }
        if(taskFound){
            addressTaskMapping[msg.sender].pop();
        }
        delete toDoLists[id];
    }
}