// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Base64.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";


contract TodoMembershipNft is ERC721Enumerable, ERC721URIStorage, Ownable {

    using Counters for Counters.Counter;
    Counters.Counter private tokenIds;

    uint32 totalNFTMinted = 0;
    uint256 membershipPrice = 0.001 ether;
    uint32 freeMemberships = 1000; 

    string firstPart = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="1024" height="512" viewBox="0 0 1024 512" xml:space="preserve"><desc>Created with Fabric.js 4.2.0</desc><defs></defs><g transform="matrix(1 0 0 1 512 256)" id="14f324b6-7bf0-4b92-be8a-aff70298aab6" ><rect style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(45,73,73); fill-rule: nonzero; opacity: 1;" vector-effect="non-scaling-stroke" x="-512" y="-256" rx="0" ry="0" width="1024" height="512"/></g><g transform="matrix(Infinity NaN NaN Infinity 0 0)" id="2c036752-aadd-424c-9437-54c3b7c4722b" ></g><g transform="matrix(1 0 0 1 512 118.64)" style="" id="8f115762-e327-469f-a787-30b9eacb82d3" ><text xml:space="preserve" font-family="Raleway" font-size="95" font-style="normal" font-weight="900" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(145,215,105); fill-rule: nonzero; opacity: 1; white-space: pre;" ><tspan x="-221.4" y="29.84" >ToDo App</tspan></text></g><g transform="matrix(1 0 0 1 512 201.82)" style="" id="fdcc3f07-037b-4c5a-b1de-d4fe588dccf7" ><text xml:space="preserve" font-family="Lato" font-size="28" font-style="normal" font-weight="400" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(255,255,255); fill-rule: nonzero; opacity: 1; white-space: pre;" ><tspan x="-152.91" y="8.8" >M</tspan><tspan x="-124.21" y="8.8" >E</tspan><tspan x="-105" y="8.8" >M</tspan><tspan x="-76.3" y="8.8" >B</tspan><tspan x="-55.24" y="8.8" >E</tspan><tspan x="-36.04" y="8.8" >R</tspan><tspan x="-15.06" y="8.8" >S</tspan><tspan x="2.72" y="8.8" >H</tspan><tspan x="26.82" y="8.8" >I</tspan><tspan x="38.36" y="8.8" >P</tspan><tspan x="58.41" y="8.8" style="white-space: pre; "> </tspan><tspan x="66.75" y="8.8" >C</tspan><tspan x="88.87" y="8.8" >A</tspan><tspan x="110.85" y="8.8" >R</tspan><tspan x="131.82" y="8.8" >D</tspan></text></g><g transform="matrix(7.05 0 0 1.09 522.54 384.62)" id="00826e8a-6e79-4596-839d-d017067d814e" ><rect style="stroke: rgb(0,0,0); stroke-width: 0; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(145,215,105); fill-rule: nonzero; opacity: 1;" vector-effect="non-scaling-stroke" x="-33.0835" y="-33.0835" rx="0" ry="0" width="66.167" height="66.167"/></g><g transform="matrix(1 0 0 1 512 381.05)" style="" id="a0b6a436-5fad-4211-8826-d114c8f236ed" ><text xml:space="preserve" font-family="Lato" font-size="40" font-style="normal" font-weight="400" style="stroke: none; stroke-width: 1; stroke-dasharray: none; stroke-linecap: butt; stroke-dashoffset: 0; stroke-linejoin: miter; stroke-miterlimit: 4; fill: rgb(0,0,0); fill-rule: nonzero; opacity: 1; white-space: pre;" ><tspan x="-105.36" y="12.57" >Member no. ';
    string secondPart = '</tspan></text></g></svg>';

    constructor () ERC721("Todo Membership NFT", "Todo") {}

    event newNFTMinted(address sender, uint256 tokenId);

    function getTotalNFTMinted() public view returns(uint32){
        return totalNFTMinted;
    }

    function getMembershipPrice() public view returns(uint256){
        return totalNFTMinted < freeMemberships ? 0 : membershipPrice;
    }

    modifier validatePrice {
        if((totalNFTMinted + 1) > freeMemberships ){
            require(msg.value >= membershipPrice, "Not enough ether");
        }
        _;
    }

    function mint(address to) internal validatePrice{
        uint256 newItemId = tokenIds.current();
        string memory memberId = Strings.toString(newItemId + 1);

        string memory name = string(abi.encodePacked("Todo #", memberId));

        string memory finalSvg = string(abi.encodePacked(firstPart, memberId, secondPart));
        
        string memory json = Base64.encode(
            bytes(
                string(
                    abi.encodePacked(
                        '{"name": "',
                        name,
                        '", "description": "A membership NFT that allows access to ToDo Dapp.", "image": "data:image/svg+xml;base64,',
                        Base64.encode(bytes(finalSvg)),
                        '"}'
                    )
                )
            )
        );

        string memory finalTokenUri = string(
            abi.encodePacked("data:application/json;base64,", json)
        );

        _safeMint(to, newItemId);
        _setTokenURI(newItemId, finalTokenUri);
        ++totalNFTMinted;
        tokenIds.increment();
        emit newNFTMinted(msg.sender, newItemId);
    }

    function mintNft() public payable{
        mint(msg.sender);
    }
 
    function mintNftTo(address to) public onlyOwner payable{
        mint(to);
    }

    function setMembershipPrice(uint256 price) public onlyOwner {
        membershipPrice = price;
    }

    function setFreeMemberships(uint32 no) public onlyOwner {
        freeMemberships = no;
    }

    // The following functions are overrides required by Solidity.

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
} 