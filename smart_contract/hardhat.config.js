require('dotenv').config();
require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-ethers");
require("@nomiclabs/hardhat-etherscan");


module.exports = {
  solidity: {
    version: "0.8.4",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
  defaultNetwork: 'matic',
  networks: {
    hardhat: {
    },
    goerli : {
      url : process.env.GOERLI_API,
      accounts : [process.env.PRIVATE_KEY]
    },
    rinkeby : {
      gas: 2100000,
      gasPrice: 8000000000,
      url : process.env.RINKEBY_API,
      accounts : [process.env.PRIVATE_KEY]
    },
    matic : {
      url : process.env.MATIC_API,
      accounts : [process.env.PRIVATE_KEY]
    }
  },
  etherscan: {
    apiKey: process.env.POLYGONSCAN_API_KEY
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },
};
