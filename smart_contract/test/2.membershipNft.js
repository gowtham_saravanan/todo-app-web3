const {expect} = require('chai');

describe('Membership NFT', () => {

    let nft;
    let accounts;
    let name ='Todo Membership NFT';
    let symbol = 'Todo'; 

    beforeEach(async () => {
        accounts = await ethers.getSigners();     
        const Nft = await ethers.getContractFactory("TodoMembershipNft");
        nft = await Nft.deploy();
    });

    it('Has valid name', async () => {
        expect(await nft.name(), name);
    })

    it('Has valid symbol', async () => {
        expect(await nft.symbol(), symbol);
    })

    it('Membership pricing works as expected', async () => {
        let price = ethers.utils.parseUnits('100', 'gwei');
        await nft.setFreeMemberships(1);
        await nft.setMembershipPrice(price);

        let membershipPrice = await nft.getMembershipPrice();
        expect(membershipPrice).to.be.equal(0);
        await nft.mintNft();

        membershipPrice = await nft.getMembershipPrice();
        expect(membershipPrice).to.be.equal(price);
        await expect(nft.mintNft()).to.be.revertedWith('Not enough ether');        
        expect(await nft.getTotalNFTMinted()).to.be.equal(1);

        await nft.mintNft({value: price});
        expect(await nft.getTotalNFTMinted()).to.be.equal(2);
    });

    it('Minting works as expected', async () => {
        await nft.mintNft();
        
        let uri = await nft.tokenURI(0);
        expect(uri).to.have.string('data:application/json;base64');
    })

})