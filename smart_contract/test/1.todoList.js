const {expect} = require('chai');

describe("TodoList Contract", () => {

    let todoList;
    let accounts;

    beforeEach(async () => {
        accounts = await ethers.getSigners();     
        const TodoList = await ethers.getContractFactory("TodoList");
        todoList = await TodoList.deploy();
    })

    async function addTask(task) {
        await todoList.add(task);
    }

    it('Successfully add and get task', async () => {
        await addTask("testing");
        let task = await todoList.getTask(1);
        
        expect(task.task).to.equal("testing");
        expect(task.owner).to.equal(accounts[0].address);
        expect(task.status).to.equal(0);
    });

    it('Successfully list all todos', async () => {
        await addTask("task 1");
        await addTask("task 2");

        let todos = await todoList.getMyLists();
        expect(todos).to.be.an('array');
        expect(todos.length).to.equal(2);

        expect(todos[0].task).to.equal("task 1");
        expect(todos[0].id).to.equal(1);

        expect(todos[1].task).to.equal("task 2");
        expect(todos[1].id).to.equal(2);
    });

    it('Successfully update the task and status', async () => {
        await addTask("task 1");
        await todoList.updateTask(1, "task 2", 2);

        let task = await todoList.getTask(1);
        expect(task.task).to.equal('task 2');
        expect(task.status).to.equal(2);
    });

    it('Successfully delete the task', async () => {
        await addTask("task 1");
        let tasks = await todoList.getMyLists();
        expect(tasks.length).to.be.equal(1);

        await todoList.deleteTask(1);
        tasks = await todoList.getMyLists();
        expect(tasks.length).to.be.equal(0);
    });

    it('Only owner can update or delete their task', async () => {
        await addTask("task 1");

        await expect(todoList.connect(accounts[1]).updateTask(1, "task 2", 2)).to.be.revertedWith('Permission Denied');
        await expect(todoList.connect(accounts[1]).deleteTask(2)).to.be.revertedWith('Permission Denied');
    });

})