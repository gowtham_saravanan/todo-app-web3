const {expect} = require('chai');

describe('Todo Token', () =>  {

    let name = 'Todo';
    let symbol = 'TD';
    let token;
    let accounts;

    beforeEach(async () => {
        accounts = await ethers.getSigners();
        const Token = await ethers.getContractFactory('TodoToken');
        token = await Token.deploy();
    });

    it('Has valid name', async () => {
        expect(await token.name()).to.be.equal(name);
    })

    it('Has valid symbol', async () => {
        expect(await token.symbol()).to.be.equal(symbol);
    })

    it('Has minted token to owner', async () => {
        expect(await token.balanceOf(accounts[0].address)).to.be.equal(ethers.utils.parseEther('1000000'))
    })

})