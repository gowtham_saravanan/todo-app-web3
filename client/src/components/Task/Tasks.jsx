import { useEffect } from "react";
import { useContext } from "react";
import {getTasks} from "../../actions/Todo";
import { AuthContext } from "../../context/AuthContext";
import Task from "./Task";

const Tasks = () =>{
    const context = useContext(AuthContext);

    useEffect(() => {
        getTasks(context);
    }, [context.currentAccount, context.network])
    
    return ( 
        <div className="pt-5 pb-10 flex flex-wrap justify-center">
            {
                context.tasks && context.tasks.map((task, index) => <Task task={task} key={index}/>)
            }
        </div>
    );
}

export default Tasks;