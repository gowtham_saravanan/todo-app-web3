import { useContext, useState, useEffect } from "react";
import { AuthContext } from "../../context/AuthContext";
import { createTask } from "../../actions/Todo";

const CreateTask = () => {

    const [task, setTask] = useState('');
    const context = useContext(AuthContext);

    const submit = (e) => {
        e.preventDefault();
        createTask(context, task);
    }

    useEffect(() => {
        switch(context.taskCreateStatus.status) {
            case "completed" : 
                setTask('');
                context.setTaskCreateStatus({status: 'pending'});
                break;
            
            case "failed": 
                context.setTaskCreateStatus({status: 'pending'});
                break;

        }
    }, [context.taskCreateStatus])
    

    return (  
       <div className="flex justify-center mx-5 my-2 md:m-8">
           <div className="bg-white w-full md:w-2/3 p-5 shadow-md rounded-xl">
                <form onSubmit={(e) => submit(e)} className="grid grid-cols-6 gap-4 items-end">
                    <div className="col-span-4 sm:col-span-5">
                        <label className="input-label">Task Name</label>
                        <input type="text" id="name" className="input-field" placeholder="To complete smart contract" required
                        value={task} onChange={(e) => setTask(e.target.value)}/>
                    </div>
                    <button type="submit" className="btn btn-blue h-10 col-span-2 sm:col-span-1" disabled={context.taskCreateStatus.loading}>
                        {context.taskCreateStatus.loading ? 'Adding...' : 'Add'}
                    </button>
                </form>
           </div>
       </div>
    );
}

export default CreateTask;