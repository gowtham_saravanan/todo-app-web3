import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClose, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons'
import { useContext, useEffect, useState } from 'react';
import { AuthContext } from '../../context/AuthContext';
import { deleteTask, editTask } from '../../actions/Todo';


const Task = (props) =>{
    const [editTask, setEditTask] = useState(false);
    
    return (  
        <>
            {editTask ?  <EditTask {...props} setEditTask={setEditTask}/> : <ViewTask {...props} setEditTask={setEditTask}/>}
        </>
    );
}


const ViewTask = (props) => {
    const context = useContext(AuthContext);

    const statuses = ["Pending", "InProgress", "Completed"];
    const statusColors = ['bg-green-100', 'bg-green-300', 'bg-blue-300']

    const onDelete = (e) => {
        e.preventDefault();
        deleteTask(context, props.task.id)
    }

    return (
        <div className='w-full sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/5 mx-5 my-2 sm:mx-2 lg:m-5 bg-white p-4 rounded-xl shadow-md flex flex-col justify-between'>
            <h5 className='text-center'>
                {props.task.task}   
            </h5>
            <div className="grid grid-cols-6 mt-4 items-center">
                <div className='col-span-3 text-center'>
                    <span className={"px-3 text-sm py-1 rounded-xl " + statusColors[props.task.status]}>
                        {statuses[props.task.status]}
                    </span>
                </div>
                <span className="col-span-1 text-blue-500" onClick={() => props.setEditTask(true)}><FontAwesomeIcon icon={faEdit}/></span>
                <div className='col-span-2 text-center'>
                    <form onSubmit={(e) => onDelete(e)}>
                        <button type="submit" className="btn btn-red text-center" disabled={context.taskDeleteStatus.loading}>
                                {context.taskDeleteStatus.loading && context.taskDeleteStatus.id == props.task.id ? 'Deleting...' : 'Delete'}
                        </button>
                    </form>
                </div>    
            </div>
        </div>
    );
}

const EditTask = (props) => {

    const context = useContext(AuthContext);
    const [task, setTask] = useState(props.task.task);
    const [status, setStatus] = useState(props.task.status);
    

    const submit = (e) => {
        e.preventDefault();
        editTask(context, props.task.id, task, status)
    }

    useEffect(() => {
        if(!context.taskUpdateStatus.id == props.task.id) return

        switch(context.taskUpdateStatus.status) {
            case "completed" : 
                props.setEditTask(false);
                context.setTaskUpdateStatus({loading : false, id : 0, status: 'pending'})
                break;
            
            case "failed": 
                context.setTaskUpdateStatus({loading : false, id : 0, status: 'pending'})
                break;
        }
    }, [context.taskUpdateStatus])
    
    return (
        <div className='w-full sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/5 mx-5 my-2 sm:mx-2 lg:m-5 bg-white p-4 rounded-xl shadow-md'>
            <form className="" onSubmit={(e) => submit(e)}>
                    <textarea rows="2" id="name" className="input-field" placeholder="To complete smart contract" required
                    value={task} onChange={(e) => setTask(e.target.value)}></textarea>
                    <div className='grid grid-cols-2 items-end'>
                        <select id="status" className="input-field col-span-1 mt-4" placeholder="Select Status" required 
                        onChange={(e) => setStatus(e.target.value)} value={status}>
                            <option value="0">Pending</option>    
                            <option value="1">In Progress</option>    
                            <option value="2">Completed</option>    
                        </select>
                        <div className='col-span-1 text-center'>
                            <button type="submit" className="btn btn-blue px-1.5 h-10 text-center" disabled={context.taskUpdateStatus.loading}>
                                {context.taskUpdateStatus.loading && context.taskUpdateStatus.id == props.task.id ? 'Updating...' : 'Update'}
                            </button>
                            <span className="ml-2 text-red-500" onClick={() =>  props.setEditTask(false)}><FontAwesomeIcon icon={faClose}/></span>
                        </div>
                    </div>
                </form>
        </div>
    );
}

export default Task;