import { Header, Loader } from "./Common";
import CreateTask from "./Task/CreateTask";
import Tasks from "./Task/Tasks";

const Home = () =>{

    return (
        <div className="w-full min-h-screen bg-slate-100">
            <Header/>
            <CreateTask/>
            <Tasks/>
        </div>
    );
}

export default Home;