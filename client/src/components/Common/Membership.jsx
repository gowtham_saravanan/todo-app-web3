import { useState } from "react";
import { useContext } from "react";
import { useEffect } from "react";
import Confetti from 'react-confetti'
import { checkMembership, getContract } from "../../actions/MembershipNft";
import { AuthContext } from "../../context/AuthContext";
import { Buffer } from 'buffer';

const Membership = () => {
    const context = useContext(AuthContext);
    const [badge, setBadge] = useState('');

    const getMembershipBadge = async () => {
        let tokenURI = await getContract(context).tokenURI(context.membershipTokenId);
        setBadge(JSON.parse(Buffer.from(tokenURI.slice(29), 'base64').toString()).image);    
    }

    useEffect(() => {
        getMembershipBadge();
        checkMembership(context);
        
        setTimeout(() => {
            context.setMintNftStatus({'status' : 'pending'})
        }, 5000)
    });


    return (
        <div>
            <Confetti numberOfPieces={50}/>
            <div className="h-screen flex flex-col justify-center items-center">
                <div className="pb-40 text-4xl font-bold text-blue-800">
                    Congrats!!! Membership NFT Minted
                </div>
                { badge && <img className="animate-bounce" src={badge} width={450}/> }
            </div>
        </div>
    )

}

export default Membership;