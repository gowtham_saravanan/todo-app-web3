import { useEffect } from "react";
import { useContext } from "react";
import { AuthContext } from "../../context/AuthContext";
import { DAOLink, NftAddress, nftBaseLink } from "../../utils/constants";

const Header = () =>{
    const {balance, currentAccount, membershipTokenId} = useContext(AuthContext);

    return (  
        <div className="pt-4 p-2 md:grid grid-cols-8 justify-center items-center">
            <div className="flex justify-center items-center col-span-2 invisible md:visible">
                <a href={DAOLink} target="_blank"><span className="mx-3 font-medium underline">Visit DAO</span></a>
                <a href={nftBaseLink + '/' +  NftAddress + '/' + membershipTokenId} target="_blank"><span className="mx-3 font-medium underline">Membership NFT</span></a>
            </div>
            <div className="flex flex-col items-center ml-3 md:ml-0 md:col-start-3 col-span-4">
                <div className="flex items-center">
                    <img className="w-16" src="/logo.png"></img>
                    <span className="text-4xl text-blue-800">ToDo</span>
                </div>
                <p className="text-lg"> Effortlessly manage your daily tasks </p>
            </div>

            <div className="flex m-3 p-2 rounded-3xl bg-white justify-center items-center text-sm md:col-start-7 col-span-2 md:w-fit">
                <span className="rounded-full w-2 h-2 bg-green-400 mx-1"></span>
                <span className="mx-1">{Number(balance).toFixed(4)} MATIC</span>
                <span className="bg-red-100 p-2 rounded-3xl mx-1">{currentAccount.slice(0, 5) + '....' + currentAccount.slice(currentAccount.length-5, currentAccount.length)}</span> 
            </div>

            <div className="flex justify-center items-center col-span-2 visible md:invisible">
                <a href={DAOLink}><span className="mx-3 font-medium underline">Visit DAO</span></a>
                <a><span className="mx-3 font-medium underline">Membership NFT</span></a>
            </div>
        </div>
    );
}

export default Header;