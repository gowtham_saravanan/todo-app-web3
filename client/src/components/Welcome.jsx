import { useContext } from "react";
import { mintNft } from "../actions/MembershipNft";
import { AuthContext } from "../context/AuthContext";
import { DAOLink, nftColletionLink } from "../utils/constants";

const Welcome = () =>{
    const {connectToWallet, currentAccount, network, isMember, mintNftStatus } = useContext(AuthContext);
    const context = useContext(AuthContext); 
    const mint = () => {
        mintNft(context);
    }
    return (  
        <div className="welcome-container px-5 md:px-0 shadow-md">
            <div className="grid grid-cols-1 lg:grid-cols-2 w-full md:w-2/3 h-2/3 bg-white ">
                <div className="flex hidden lg:block">
                    <img className="w-full py-10" src="task-vector.jpg"/>   
                </div>
                <div className="flex flex-col items-center justify-center p-5">
                    <img src="logo.png"  className="w-1/4 mb-5"/>
                    <p className="font-bold text-2xl">
                        Welcome to ToDo
                    </p>
                    <p className="mt-2 font-ligth text-md ">
                        Effortlessly manage your daily tasks 
                    </p>
                    {
                        !window.ethereum &&
                        <span className="mt-4 text-red-500 bg-gray-200 p-2 rounded-lg">
                            Kindly setup <a target="_blank" href="https://metamask.io/" className="font-bold text-blue-500 underline">metamask</a> to use this application. 
                        </span>
                    }
                    {   
                        window.ethereum && currentAccount == '0x' &&
                        <button className="mt-10 btn btn-blue" onClick={() => connectToWallet()}>
                            Connect Wallet
                        </button>
                    }
                    {
                        window.ethereum && currentAccount != '0x' && network.chainId != 137 &&
                        <span className="mt-4 text-red-500 bg-gray-200 p-2 rounded-lg">
                            Currently connected to <span className="font-bold">{(network) && network.name}</span>. Switch to <span className="font-bold">Polygon (Matic)</span> to continue 
                        </span>
                    }
                    {
                        window.ethereum && currentAccount != '0x' &&  !isMember && network.chainId == 137 && 
                        <button className="mt-10 btn btn-blue" onClick={mint} disabled={mintNftStatus.loading}>
                            {mintNftStatus.loading ? 'Please Wait. Minting...' : 'Mint Membership NFT to proceed'}
                        </button>
                    }
                    
                    <div className="mt-10 text-sm text-blue-600" >
                        <div className="border-t-2 mb-5 w-100"></div>
                        <div className="flex items-center">
                            <a href={DAOLink} target="_blank" className="mx-2"><img src="/dao.png" width={50} /></a>
                            <a href={nftColletionLink} target="_blank" className="mx-2"><img src="/opensea.jpg" width={30} /></a>
                            <a href={'https://gitlab.com/gowtham_saravanan/todo-app-web3'} target="_blank" className="mx-2"><img src="/github.jpg" width={30} /></a>
                            <a href={'https://www.linkedin.com/in/gowtham-saravanan/'} target="_blank" className="mx-2"><img src="/linkedin.png" width={30} /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Welcome;