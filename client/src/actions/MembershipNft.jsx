import { nftAbi, NftAddress } from "../utils/constants";
import {ethers} from 'ethers';

export const getContract = (context) => {
    const singer = context.getProvider().getSigner();
    return new ethers.Contract(NftAddress, nftAbi, singer);
}

export const checkMembership = async (context) => {
    try{
        if(context.currentAccount == '0x') return; 
        const balance = await getContract(context).balanceOf(context.currentAccount);
        context.setIsMember(balance > 0);
    }catch (error){
        console.log(error);
        context.setAlert({show: true, 'message' : error.message})
    }
}

export const getMembershipPrice = async (context) => {
    try{
        if(context.currentAccount == '0x') return; 
        context.setMembershipPrice(ethers.BigNumber.from(await getContract(context).getMembershipPrice()).toNumber());
    }catch (error){
        console.log(error);
        context.setAlert({show: true, 'message' : error.message})
    }
}

export const mintNft = async (context) => {
    if(context.currentAccount == '0x') return; 
    const options = {value: context.membershipPrice}
    try{
        context.setMintNftStatus({loading : true})
        const transaction = await getContract(context).mintNft(options);
        let mint = await transaction.wait()
        let {owner, tokenId} = mint.events.find(e => e.event == 'newNFTMinted').args;
        if(owner == context.currentAccount){
            context.setMembershipTokenId(tokenId);
        }
        context.setMintNftStatus({loading : false, status : 'completed'})
    }catch (error){
        console.log(error);
        context.setAlert({show: true, 'message' : (error.code == 4001 ? 'Transaction Failed' : error.message)})
        context.setMintNftStatus({loading : false, status : 'failed'})
    }
}

export const getMembershipId = async (context) => {
    if(context.currentAccount == '0x') return; 
    context.setMembershipTokenId(await getContract(context).tokenOfOwnerByIndex(context.currentAccount, 0));
}