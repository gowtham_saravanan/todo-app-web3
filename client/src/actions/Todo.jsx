import { todoAbi, todoAddress } from "../utils/constants";
import {ethers} from 'ethers';

export const getContract = (context) => {
    const singer = context.getProvider().getSigner();
    return new ethers.Contract(todoAddress, todoAbi, singer);
}

export const getTasks = async (context) => {
    try{
        const tasks = await getContract(context).getMyLists();
        context.setTasks(tasks);
    }catch (error){
        context.setAlert({show: true, 'message' : error.message})
    }
}

export const createTask = async (context, task) => {
    try{
        context.setTaskCreateStatus({loading : true})
        const transaction = await getContract(context).add(task);
        await transaction.wait()
        getTasks(context);
        context.setTaskCreateStatus({loading : false, status : 'completed'})
        context.setAlert({show: true, 'message' : 'New task created successfully'})
    }catch (error){
        context.setAlert({show: true, 'message' : (error.code == 4001 ? 'Transaction Failed' : error.message)})
        context.setTaskCreateStatus({loading : false, status : 'failed'})
    }
}

export const editTask = async (context, id, task, status) => {    
    try{
        context.setTaskUpdateStatus({loading : true, id})
        const transaction = await getContract(context).updateTask(Number(id), task, status);
        await transaction.wait()
        getTasks(context);
        context.setTaskUpdateStatus({loading : false, id, status: 'completed'})
        context.setAlert({show: true, 'message' : 'Update successful'})
    }catch (error){
        context.setAlert({show: true, 'message' : (error.code == 4001 ? 'Transaction Failed' : error.message)})
        context.setTaskUpdateStatus({loading : false, id, status: 'failed'})
    }
}

export const deleteTask = async (context, id) => {
    try{
        context.setTaskDeleteStatus({loading : true, id})
        const transaction = await getContract(context).deleteTask(Number(id));
        await transaction.wait()
        getTasks(context);
        context.setTaskDeleteStatus({loading : false, id: 0})
        context.setAlert({show: true, 'message' : 'Delete successful'})
    }catch (error){
        context.setAlert({show: true, 'message' : (error.code == 4001 ? 'Transaction Failed' : error.message)})
        context.setTaskcontext.DeleteStatus({loading : false, id, status: 'failed'})
    }
}
