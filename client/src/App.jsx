import Welcome from "./components/Welcome"
import Home from "./components/Home"
import { useContext, useEffect, useState } from "react"
import { AuthContext } from "./context/AuthContext"
import Alert from "./components/Common/Alert"
import { checkMembership, getMembershipId, getMembershipPrice } from "./actions/MembershipNft"
import Membership from "./components/Common/Membership"

const App = () => {  

  const context = useContext(AuthContext)
  const [loader, setLoader] = useState(true);

  useEffect(() => {
      checkMembership(context);
      getMembershipPrice(context);
      getMembershipId(context);
  }, [context.currentAccount, context.network])


  setTimeout(() => {
    setLoader(false)
  }, 2000)

  if(context.mintNftStatus.status == 'completed' && context.membershipTokenId != -1){
      return <Membership/>;
  }

  if(loader){
    return (
      <div className="min-h-screen flex justify-center items-center">
          <img className="w-1/2 md:w-1/4" src="/ethereum.gif"/>
      </div>
    );
  }

  return (
      <>
        <Alert/>
        {window.ethereum && context.isWalletConnected() && context.isMember ? <Home/> : <Welcome/>}
      </>
  )
}

export default App
