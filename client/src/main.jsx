import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { TransactionProvider } from './context/AuthContext'
import './css/index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <TransactionProvider>
    <React.StrictMode>
      <App />
      <div className='w-fit fixed bottom-5 right-5 opacity-60'>
        <img className='w-16 align-right' src='/polygon.png'/>
      </div>
    </React.StrictMode>
   </TransactionProvider>
)
